﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    private Sprite[] animationSprites;
    private int indexSprite;

    //Save sprite var to spriteRenderer
    public SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        animationSprites = gameObject.GetComponent<DataPlayer>().Sprites;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        spriteRenderer.sprite = animationSprites[0];
        Debug.Log("hola "+spriteRenderer.sprite.name);

        indexSprite = 0;

        Application.targetFrameRate = 12;

    }

    // Update is called once per frame
    void Update()
    {
        if (indexSprite == 0) {

            indexSprite = 1;

        } else if (indexSprite == 1) {

            indexSprite = 2;
        }
        else if (indexSprite == 2)
        {

            indexSprite = 0;
        }

        spriteRenderer.sprite = animationSprites[indexSprite];


    }
}
