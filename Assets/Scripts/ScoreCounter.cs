﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    private Text scoreText;
    private GameObject gm; 
    // Start is called before the first frame update
    void Start()
    {
        scoreText = GetComponent<Text>();
        gm = GameObject.Find("GameManager");

    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "SCORE: "+ gm.GetComponent<GameManager>().score;
    }
}
