﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbilities : MonoBehaviour
{
    [SerializeField]
    private Vector3 scale;
    private DataPlayer player;
    private Vector3 scaleMaxSize;
    private Vector3 scaleMinSize;
    private float timeRemaining;
    private float timeRemainingColdown;

    private enum HGStatus
    {
        idle,
        grow,
        giantState,
        unGrow,
        coldown
    }

    private HGStatus currentState;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<DataPlayer>();

        scale = transform.localScale;
        scaleMaxSize = (scale + new Vector3(1,1,1)) * player.Height;
        scaleMinSize = scale;

        timeRemaining = 5;
        timeRemainingColdown = 10;

        currentState = HGStatus.idle;
    }

    // Update is called once per frame
    void Update()
    {


        switch (currentState)
        {

            case HGStatus.idle:

                if (Input.GetKeyDown("space"))
                {
                    currentState = HGStatus.grow;

                }

                break;

            case HGStatus.grow:
                Grow();
                break;

            case HGStatus.giantState:
                giantState();
                break;

            case HGStatus.unGrow:
                unGrow();
                break;

            case HGStatus.coldown:
                Coldown();
                break;
        }



    }

     void Coldown()
    {
        if (timeRemainingColdown > 0)
        {
            timeRemainingColdown -= Time.deltaTime;
        }
        else
        {
            timeRemainingColdown = 10;
            currentState = HGStatus.idle;
        }

    }

    void Grow()
    {
        bool growing = true;

        if (growing) {

            transform.localScale += new Vector3(1, 1, 1);

            if (transform.localScale.x >=  scaleMaxSize.x ) {
                growing = false;
                currentState = HGStatus.giantState;

            }
        }

    }

    void unGrow()
    {
        bool unGrowing = true;

        if (unGrowing)
        {

            transform.localScale -= new Vector3(1, 1, 1);

            if (transform.localScale.x <= scaleMinSize.x )
            {
                unGrowing = false;
                currentState = HGStatus.coldown;

            }
        }

    }

    void giantState()
    {                 

            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                timeRemaining = 5;
                currentState = HGStatus.unGrow;
            }
        

    }
}



