﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLives : MonoBehaviour
{
    
    private GameManager script;
    private GameObject gm;
    private Text HpText;
    // Start is called before the first frame update
    void Start()
    {
        HpText = GetComponent<Text>();
        gm = GameObject.Find("GameManager");
        script = gm.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        HpText.text = "HP: " + script.lives;
    }
}
