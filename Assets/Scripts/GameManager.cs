﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private GameObject dp;
    private DataPlayer script;
    private Text numEnemiesText;
    public int lives;
    public int score;
    public int numEnemiesScene;


    // Start is called before the first frame update
    void Start()
    {
        dp = GameObject.Find("Player");
        numEnemiesText = GameObject.Find("NumEnemies").GetComponent<Text>();
        script = dp.GetComponent<DataPlayer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        lives = script.lives;
        Debug.Log(score);
        numEnemiesText.text = "Number of enemies: "+NumEnemies();
    }

    private int NumEnemies() {
        return numEnemiesScene = GameObject.FindGameObjectsWithTag("Enemy").Length;
    
    }


}
