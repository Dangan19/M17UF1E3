﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveControler : MonoBehaviour
{
    public Transform player;
    public float speed = 1.0f;

    // Start is called before the first frame update

    void Start()
    {
        player = GameObject.Find("Player").transform;
        //transform.position = new Vector3(2.0f, 0.0f, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, player.position, step);
    }
}
