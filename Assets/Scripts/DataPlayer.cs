﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerKind
{
    Sorcerer,
    Knight,
    Paladin,
    Assasin,
    Bard,
    Barbarian
}


public class DataPlayer : MonoBehaviour
{
    [SerializeField]
    private string Name;
    public string Surname;
    public float Speed = 10f;
    public float Height;
    public float Weight;
    public float WalkDistance;
    public PlayerKind Kind;
    public Sprite[] Sprites;
    public int lives;


    private bool movingLeft;
    private Vector3 finalPatrullaLeft;
    private Vector3 finalPatrullaRight;


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Tipo "+Name+" Surname "+Surname+" Speed "+Speed);

        //Creation of the floor
        GameObject floor = GameObject.CreatePrimitive(PrimitiveType.Plane);
        floor.transform.position = new Vector3(0.0f, -1.0f, 0.0f);

        //Vector final patrulla
        finalPatrullaLeft = new Vector3(-WalkDistance, transform.position.y, 0.0f);
        finalPatrullaRight = new Vector3(WalkDistance, transform.position.y, 0.0f);

        movingLeft = true;




    }

    // Update is called once per frame
    void Update()
    {
        Patrol();

    }

    void Patrol()
    {
        if (movingLeft == true)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            transform.Translate(Vector3.left * Time.deltaTime * Speed/(Weight * 10));

            if (transform.position.x <= finalPatrullaLeft.x)
            {
                movingLeft = false;

            }
        }
        else {

            transform.localRotation = Quaternion.Euler(0, 180, 0);
            transform.Translate(Vector3.left * Time.deltaTime * Speed/(Weight*10));


            if (transform.position.x >= finalPatrullaRight.x)
            {
                movingLeft = true;

            }
        }

    }

}
