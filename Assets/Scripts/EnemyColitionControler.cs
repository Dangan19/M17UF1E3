﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EnemyColitionControler : MonoBehaviour
{
    //ColitionControler with enemies and player
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //Code to player collision
            Debug.Log("Coli amb player");

            //Aplicar damage
            GameObject pl = collision.gameObject;
            int pLives = pl.GetComponent<DataPlayer>().lives;

            if (pLives <= 0) {
                Destroy(pl);
                new WaitForSeconds(5);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
            {
                pl.GetComponent<DataPlayer>().lives--;

            }
            
        }

        Destroy(gameObject);
    }
    
}
