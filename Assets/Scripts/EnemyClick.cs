﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyClick : MonoBehaviour
{
    [SerializeField] private int enemyPoints = 5;

    
    void OnMouseDown()
    {
        GameObject gm = GameObject.Find("GameManager");
         gm.GetComponent<GameManager>().score+=enemyPoints;

        Destroy(gameObject);

    }
}
